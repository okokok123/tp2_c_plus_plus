/******************************************************************************
	Fichier:	donnees_simulation.h

	Auteur:		St�phane Lapointe

	Utilit�:	D�claration des constantes contenant l'information
				n�cessaire pour faire une simulation.
******************************************************************************/
#ifndef __DONNEES_SIMULATION_H
#define __DONNEES_SIMULATION_H

// Nombre d'�tapes de la simulation.
const unsigned char NB_ETAPES = 28;

/*
Types d'�tapes pour la simulation :
	1 : Signifie qu'il faut ajouter une t�che au projet.
	2 : Signifie qu'il faut assigner une t�che � un employ�.
	3 : Signifie qu'un employ� a compl�t� la t�che qui lui �tait assign�e.
*/

/*
Informations pour les �tapes de la simulation :
	Premier champ : Type d'�tape.
	Champs suivants en fonction du type d'�tape:
		type 1 : {NOM_TACHE, PRIORIT�, NIVEAU_EXP�RIENCE_MINIMAL}.
		types 2 et 3 : {NUM�RO D'EMPLOY�, NON_UTILIS�, NON_UTILIS�}.
*/
const char* ETAPES_SIMULATION [NB_ETAPES] [4] =
{
	// Ajout de 5 t�ches au projet (aucune erreur possible ici).
	{"1", "tache-1", "2", "3"}, // �tape 1
	{"1", "tache-2", "3", "5"}, // �tape 2
	{"1", "tache-3", "2", "1"}, // �tape 3
	{"1", "tache-4", "1", "5"}, // �tape 4
	{"1", "tache-5", "1", "2"}, // �tape 5

	// Assignation de t�ches � des employ�s.
	{"2", "7301", "", ""}, // �tape 6  : Succ�s : "tache-4" assign�e � l'employ�.
	{"2", "7308", "", ""}, // �tape 7  : Succ�s : "tache-2" assign�e � l'employ�.
	{"2", "7305", "", ""}, // �tape 8  : Succ�s : "tache-1" assign�e � l'employ�.
	{"2", "7399", "", ""}, // �tape 9  : Erreur : No d'employ� invalide.
	{"2", "7301", "", ""}, // �tape 10 : Erreur : L'employ� a d�j� une t�che assign�.
	{"2", "7306", "", ""}, // �tape 11 : Erreur : Aucune t�che avec le bon NEM parmi celles disponibles.
	{"2", "7303", "", ""}, // �tape 12 : Succ�s : "tache-5" assign�e � l'employ�.
	{"2", "7302", "", ""}, // �tape 13 : Succ�s : "tache-3" assign�e � l'employ�.
	{"2", "7304", "", ""}, // �tape 14 : Erreur : Il n'y a plus aucune t�che de disponible.

	// Compl�tion de t�ches par des employ�s.
	{"3", "7302", "", ""}, // �tape 15 : Succ�s
	{"3", "7308", "", ""}, // �tape 16 : Succ�s
	{"3", "7303", "", ""}, // �tape 17 : Succ�s
	{"3", "7388", "", ""}, // �tape 18 : Erreur : No d'employ� invalide.
	{"3", "7302", "", ""}, // �tape 19 : Erreur : Aucune t�che assign�e � cet employ�.
	{"3", "7301", "", ""}, // �tape 20 : Succ�s
	{"3", "7305", "", ""}, // �tape 21 : Succ�s : Tous les employ�s ont compl�t�s leur t�che assign�e.

	// Ajout de 5 t�ches et de 2 assignations � des employ�s (aucune erreur ici).
	{"1", "tache-6", "3", "4"}, // �tape 22
	{"1", "tache-7", "2", "5"}, // �tape 23
	{"1", "tache-8", "3", "1"}, // �tape 24
	{"1", "tache-9", "1", "2"}, // �tape 25
	{"1", "tache10", "2", "4"}, // �tape 26
	{"2", "7308", "", ""}, // �tape 27 : Succ�s
	{"2", "7301", "", ""} // �tape 28 : Succ�s
};

#endif

