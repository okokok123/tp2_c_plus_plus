/******************************************************************************
	Fichier:	tache.cpp

	Classe:		CTache

	Auteur:		St�phane Lapointe

	Utilit�:	Impl�mentation des m�thodes de la classe CTache repr�sentant
				une t�che d'un projet.
******************************************************************************/

#include "tache.h"

/******************************************************************************
	Constructeur param�tr�.
******************************************************************************/
CTache::CTache(const string& strDescription, int iPriorite, int iNiveauExpMin)
	: m_strDescription(strDescription), m_iPriorite(iPriorite), m_iNiveauExpMin(iNiveauExpMin)
{
}

/******************************************************************************
	Permet d'obtenir la description de la t�che.
******************************************************************************/
const string& CTache::GetDescription() const
{
	return this->m_strDescription;
}

/******************************************************************************
	Permet d'obtenir la priorit� de la t�che.
******************************************************************************/
int CTache::GetPriorite() const
{
	return this->m_iPriorite;
}

/******************************************************************************
	Permet d'obtenir le niveau d'exp�rience minimal exig� de la t�che.
******************************************************************************/
int CTache::GetNiveauExpMin() const
{
	return this->m_iNiveauExpMin;
}

