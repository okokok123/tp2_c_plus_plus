/******************************************************************************
	Fichier:	employe.h

	Classe:		CEmploye

	Auteur:		St�phane Lapointe

	Utilit�:	D�claration de la classe CEmploye repr�sentant un employ�
				travaillant sur un projet.
******************************************************************************/

#ifndef __EMPLOYE_H
#define __EMPLOYE_H

#include <string>
#include "tache.h"

using namespace std;

class CEmploye
{
public:
	// Constructeur param�tr�.
	CEmploye(const string& strNom, int iNoEmp, int iNiveauExp, CTache* pTacheAss = NULL);

	// Constructeur copie.
	CEmploye(const CEmploye& autreEmploye);

	// Destructeur.
	~CEmploye();

	// Op�rateur d'affectation (=).
	const CEmploye& operator =(const CEmploye& autreEmploye);

	// M�thode permettant d'assigner une t�che � un employ�.
	bool AssignerTache(CTache* pTache);
	// M�thode permettant de lib�rer la t�che actuellement assign�e
	// � un employ� et de la retourner.
	CTache* LibererTache();

	// M�thodes "Get".
	const string& GetNom() const;
	int GetNoEmp() const;
	int GetNiveauExp() const;
	const CTache* GetTacheAssignee() const;

private:
	// Nom complet de l'employ�.
	string m_strNom;
	// Num�ro d'employ�.
	int m_iNoEmp;
	// Niveau d'exp�rience de l'employ� (1 � 5) :
	// 1 = Tr�s exp�riment� et 5 = junior.
	int m_iNiveauExp;
	// T�che actuellement assign�e � l'employ�.
	CTache* m_pTacheAssignee;
};

#endif

