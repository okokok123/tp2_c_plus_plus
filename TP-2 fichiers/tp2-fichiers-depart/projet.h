/******************************************************************************
	Fichier:	projet.h

	Classe:		CProjet

	Auteur:		St�phane Lapointe

	Utilit�:	D�claration de la classe CProjet repr�sentant un projet ayant
				des t�ches non assign�es, des t�ches compl�t�es et des employ�s
				pouvant accomplir des t�ches.
******************************************************************************/

#ifndef __PROJET_H
#define __PROJET_H

#include <string>
#include "employe.h"
#include "liste_chainee_taches.h"

using namespace std;

class CProjet
{
public:
	// Constructeur par d�faut.
	CProjet(const string& strNom = "non defini");
	// Constructeur copie.
	CProjet(const CProjet& autreProjet);

	// Destructeur
	~CProjet();

	// Op�rateur d'affectation (=).
	const CProjet& operator =(const CProjet& autreProjet);

	// Permet d'ajouter une nouvelle t�che non assign�e au projet.
	void AjouterTacheNonAssignee(CTache* pTache);

	// Permet d'assigner un t�che non assign�e � un employ�
	// s'il en a pas et s'il peut r�aliser une de ces t�ches.
	// Retourne true si l'op�ration est possible; false, autrement.
	bool AssignerTacheEmploye(int iNoEmp);

	// Permet de sp�cifier qu'un employ� a compl�t� la t�che
	// qui lui �tait assign�e s'il en avait une.
	// La t�che doit �tre retourn�e � la fin de la liste des t�ches
	// compl�t�s.
	// Retourne true si l'op�ration est possible; false, autrement.
	bool CompleterTacheEmploye(int iNoEmp);

	// Permet d'obtenir le nombre de t�ches actuellement
	// assign�es � des employ�s.
	int NbTachesAssignees() const;


	// NOTE : LES M�THODES D'AFFICHAGE CI-DESSOUS NE DEVRAIENT
	// NORMALEMENT PAS FAIRE PARTIE DE CETTE CLASSE.
	// ELLES SONT PAR CONTRE N�CESSAIRES POUR POUVOIR V�RIFIER
	// LES R�SULTATS DE LA SIMULATION.

	// Permet d'afficher dans la console les t�ches non assign�es.
	void AfficherTachesNonAssignees();

	// Permet d'afficher dans la console les t�ches compl�t�es.
	void AfficherTachesCompletees();

	// Permet d'afficher dans la console l'employ� correspondant au num�ro.
	// Si le num�ro d'employ� est valide, les informations de l'employ�
	// sont affich�es dans la console et true est retourn�.
	// Autrement, rien n'est affich� et false est retourn�.
	bool AfficherEmploye(int iNoEmp) const;

	// Permet d'afficher dans la console la t�che assign�e � un employ�.
	// Si le num�ro d'employ� est valide et qu'il a une t�che assign�e,
	// les informations sur la t�che assign�e sont affich�es dans la
	// console et true est retourn�.
	// Autrement, rien n'est affich� et false est retourn�.
	bool AfficherTacheEmploye(int iNoEmp) const;


private:

	// DONN�ES MEMBRES
	// ---------------
	// Nom du projet.
	string m_strNom;

	// Pointeur sur la liste cha�n�e des t�ches non assign�es.
	CListeChaineeTaches* m_pListeNonAssignees;

	// Pointeur sur la liste cha�n�e des t�ches compl�t�es.
	CListeChaineeTaches* m_pListeTachesCompletees;

	// Pointeur sur un tableau de pointeurs sur des employ�s
	// pouvant accomplir des t�ches.
	CEmploye** m_ppLesEmployes;

	// M�THODES PRIV�ES
	// ---------------
	// Permet de rechercher un employ� en fonction de son num�ro.
	// Retourne un pointeur sur l'employ� trouv� ou bien NULL si l'employ�
	// n'existe pas.
	CEmploye* RechercherEmploye(int iNoEmp) const;

	// Permet d'afficher dans la console une liste cha�n�e de t�ches.
	void AfficherListeChainee(CListeChaineeTaches& liste);
};

#endif

