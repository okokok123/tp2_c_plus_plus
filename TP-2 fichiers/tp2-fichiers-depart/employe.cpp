/******************************************************************************
	Fichier:	employe.cpp

	Classe:		CEmploye

	Auteur:		St�phane Lapointe

	Utilit�:	Impl�mentation des m�thodes de la classe CEmploye repr�sentant
				un employ� travaillant sur un projet.
******************************************************************************/

#include "employe.h"

/******************************************************************************
	Constructeur param�tr�.
******************************************************************************/
CEmploye::CEmploye(const string& strNom, int iNoEmp, int iNiveauExp, CTache* pTacheAss)
	: m_strNom(strNom), m_iNoEmp(iNoEmp), m_iNiveauExp(iNiveauExp), m_pTacheAssignee(pTacheAss)
{
}

/******************************************************************************
	Constructeur copie.
******************************************************************************/
CEmploye::CEmploye(const CEmploye& autreEmploye)
	: m_pTacheAssignee(NULL)
{
	*this = autreEmploye;
}

/******************************************************************************
	Destructeur.
******************************************************************************/
CEmploye::~CEmploye()
{
	// *** � COMPL�TER ***
}

/******************************************************************************
	Op�rateur d'affectation (=).
******************************************************************************/
const CEmploye& CEmploye::operator =(const CEmploye& autreEmploye)
{
	// *** � COMPL�TER ***
}

/******************************************************************************
	La m�thode AssignerTache(...) permet d'assigner la t�che re�ue en
	param�tre � l'employ� (s'il en avait aucune) et de retourner true.
	Si l'employ� a d�j� une t�che assign�e, l'op�ration n'est pas possible
	et la m�thode retourne false pour l'indiquer.
******************************************************************************/
bool CEmploye::AssignerTache(CTache* pTache)
{
	// *** � COMPL�TER ***
}

/******************************************************************************
	La m�thode LibererTache(...) permet de retirer � l'employ� la t�che qui
	lui est actuellement assign�e et de la retourner.  Si aucune t�che n'est
	assign�e � l'employ�, la m�thode retourne NULL.
******************************************************************************/
CTache* CEmploye::LibererTache()
{
	// *** � COMPL�TER ***
}

/******************************************************************************
	Permet d'obtenir le nom complet de l'employ�.
******************************************************************************/
const string& CEmploye::GetNom() const
{
	return this->m_strNom;
}

/******************************************************************************
	Permet d'obtenir le num�ro de l'employ�.
******************************************************************************/
int CEmploye::GetNoEmp() const
{
	return this->m_iNoEmp;
}

/******************************************************************************
	Permet d'obtenir le niveau d'exp�rience de l'employ�.
******************************************************************************/
int CEmploye::GetNiveauExp() const
{
	return this->m_iNiveauExp;
}

/******************************************************************************
	Permet d'obtenir un pointeur sur la t�che actuellement assign�e �
	l'employ�.
******************************************************************************/
const CTache* CEmploye::GetTacheAssignee() const
{
	return this->m_pTacheAssignee;
}

