/******************************************************************************
	Fichier:	liste_chainee_taches.h

	Classe:		CListeChaineeTaches et CNoeudTache

	Auteur:		St�phane Lapointe

	Utilit�:	D�claration de la classe CListeChaineeTaches et de la classe
				CNoeudTache repr�sentant une liste cha�n�e de t�ches.
******************************************************************************/

#ifndef __LISTE_CHAINEE_TACHES_H
#define __LISTE_CHAINEE_TACHES_H

#include "tache.h"

class CNoeudTache
{
	// Classe amie : permet � CListeChaineeTaches d'acc�der aux membres priv�s
	// de CNoeudTache.
	friend class CListeChaineeTaches;

public:
	// Rien de publique.

private:
	// Donn�es-membres
	// ===============
	// Pointeur sur la t�che conserv�e dans le noeud.
	CTache* m_pTache;
	// Pointeur sur le noeud suivant.
	CNoeudTache* m_pNoeudSuivant;

	// Constructeurs
	// =============
	// Constructeur param�tr�.
	CNoeudTache(CTache* pTache);
	// Constructeur copie.
	CNoeudTache(const CNoeudTache& autreNoeud);
};

class CListeChaineeTaches
{
public:

	// Constructeurs
	// =============
	// Constructeur par d�faut (sans param�tres).
	CListeChaineeTaches();
	// Constructeur copie.
	CListeChaineeTaches(const CListeChaineeTaches& autreListe);

	// Destructeur
	// ===========
	~CListeChaineeTaches();

	// Op�rateur
	// =========
	// Op�rateur d'affectation (=).
	const CListeChaineeTaches& operator = (const CListeChaineeTaches& autreListe);

	// M�thodes
	// ========
	// Permet d'ajouter une t�che dans un nouveau noeud � la fin de la
	// liste.  Le nouveau noeud devient le noeud courant.
	void Ajouter(CTache* tacheAjout);

	// Permet d'ins�rer une t�che dans un nouveau noeud dans la liste en
	// fonction de sa priorit�.  Le nouveau noeud devient le noeud courant.
	void Inserer(CTache* tacheAjout);

	/* Permet d'extraire la prochaine t�che qui devrait �tre accomplie;
	c'est-�-dire celle avec la plus grande priorit�, la plus en-t�te de
	liste et dont le niveau d'exp�rience minimal exig� est satisfait selon
	le param�tre re�u.  Si un telle t�che existe, son noeud est supprim� et
	le pointeur sur cette t�che est retourn�; autrement, aucun noeud n'est
	supprim� et la valeur NULL est retourn�e.
	Le noeud courant n'est pas d�plac� sauf s'il est positionn� sur le
	noeud � supprimer; dans un tel cas, il est repositionn� sur le premier
	noeud de la liste. */
	CTache* ExtraireTache(int iNiveauExp);

	// Permet de d�placer le noeud courant sur le premier noeud.
	void Premier();

	// Permet de d�placer le noeud courant sur le noeud suivant,
	// si possible. Retourne true si le noeud courant a �t� d�plac�
	// et false si ce n'est pas possible.
	bool Suivant();

	// Permet d'obtenir un pointeur sur la t�che du noeud courant;
	// NULL si le noeud courant n'existe pas.
	const CTache* TacheDuCourant() const;

	// Permet de vider la liste en supprimant tous les noeuds.
	void Vider();

	// Permet de v�rifier si la liste est vide.  Retourne true si c'est
	// le cas; false autrement.
	bool EstVide() const;

	// Permet d'obtenir le nombre total de t�ches dans la liste
	// (peu importe la priorit�).
	int NbTachesTotal() const;

	// Permet d'obtenir le nombre de t�ches dans la liste ayant un certain
	// niveau de priorit�.
	int NbTachesAvecPriorite(int iPriorite) const;

private:
	// Donn�es-membres
	// ===============
	// Pointeur sur le premier noeud de la liste.
	CNoeudTache* m_pPremierNoeud;
	// Pointeur sur le noeud courant de la liste.
	CNoeudTache* m_pNoeudCourant;
};

#endif

