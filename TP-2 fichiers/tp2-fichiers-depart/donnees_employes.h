/******************************************************************************
	Fichier:	donnees_employes.h

	Auteur:		St�phane Lapointe

	Utilit�:	D�claration des constantes contenant l'information
				sur les employ�s participant au projet.
******************************************************************************/

#ifndef __DONNEES_EMPLOYES_H
#define __DONNEES_EMPLOYES_H

// Nombre d'employ�es.
const unsigned char NB_EMPLOYES = 10;

// Informations pour la cr�ation des employ�s : {NOM, NUM�RO, NIVEAU D'EXP�RIENCE}.
const char* LES_EMPLOYES [NB_EMPLOYES] [3] =
{
	{"Kashmir Ducom", "7301", "1"},
	{"Zanael Batard", "7302", "1"},
	{"Azilis Tapin", "7303", "2"},
	{"Mayeul Malfait", "7304", "2"},
	{"Alexiam Castorix", "7305", "3"},
	{"Zoemy Malapry", "7306", "3"},
	{"Capri Lagarce", "7307", "1"},
	{"Samsara Gaudiche", "7308", "4"},
	{"Ghessy Grommolard", "7309", "3"},
	{"Abyalex Fayot", "7310", "5"}
};

#endif

