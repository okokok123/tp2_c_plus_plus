/******************************************************************************
	Fichier:	tache.h

	Classe:		CTache

	Auteur:		St�phane Lapointe

	Utilit�:	D�claration de la classe CTache repr�sentant une t�che
				d'un projet.
******************************************************************************/

#ifndef __TACHE_H
#define __TACHE_H

#include <string>

using namespace std;

class CTache
{
public:
	// Constructeur param�tr�.
	CTache(const string& strDescription, int iPriorite, int iNiveauExpMin);

	// M�thodes "Get".
	const string& GetDescription() const;
	int GetPriorite() const;
	int GetNiveauExpMin() const;

private:
	// Description de la t�che.
	string m_strDescription;
	// Priorit� de la t�che (de 1 � 3) :
	// 1 = �lev�e, 2 = Moyenne, 3 = Basse.
	int m_iPriorite;
	// Niveau d'exp�rience minimal exig� pour qu'un employ�
	// puisse se voir assigner cette t�che (de 1 � 5) :
	// 1 = Tr�s exp�riment� et 5 = junior.
	int m_iNiveauExpMin;
};

#endif

